--[[--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------- Начало кода. Скрипт заказан: https://vk.com/id --------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------]]

script_name('FermaPoc')
script_authors('')
script_description('')
--script_version("1.0")
scriptversion = "1"
script_properties('work-in-pause')

script_version '1.1.0'
local dlstatus = require "moonloader".download_status

--[[--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------- Проверка на либы --------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------]]

local res, moon = pcall(require, 'lib.moonloader') -- либа мунлоадера
if not res then
	print('Библиотека \"lib.moonloader\" отсутствует')
end	
assert(res, 'Библиотека \"lib.moonloader\" отсутствует')
--------------------------------------------------
local res, sampev = pcall(require, 'lib.samp.events') -- Хуки событий сампа
if not res then
	print('Библиотека \"Samp.lua\" отсутствует')
end	
assert(res, 'Библиотека \"Samp.lua\" отсутствует')
--------------------------------------------------
if getModuleHandle("samp.dll") == nil then -- Проверка на самп
	print('Запуск скрипта невозможен без SA-MP!')
	assert(false, 'Запуск скрипта невозможен без SA-MP!')
end
--------------------------------------------------
if getMoonloaderVersion() ~= 26 then -- Проверка версии мунлоадера
	print('Версия Moonloader отличается от 0.26! Запуск скрипта невозможен!')
	assert(false, "Версия Moonloader отличается от 0.26! Запуск скрипта невозможен!")
end

--[[--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------- Нужные переменные --------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------]]
local farms = {
	[1] = {
		farm = {x = 118.9506, y = 2509.6948, z = 17.0258}, 
		barn = {x = 123.6087, y = 2494.5564, z = 16.5042},
		barrel = {x = 94.4270, y = 2493.4509, z = 16.4964},
		spryatalsya = {x = 122.2763, y = 2488.4609, z = 16.5042},
		pits = {
			{x = 137.1230, y = 2488.2085, z = 16.8465},
			{x = 137.4837, y = 2495.9219, z = 16.8152},
			{x = 137.7494, y = 2502.5251, z = 16.9678},
			{x = 137.8007, y = 2509.1958, z = 16.9367},
			{x = 137.7857, y = 2516.0950, z = 16.9417},
			{x = 145.6927, y = 2516.0039, z = 16.9595},
			{x = 145.7911, y = 2509.4121, z = 16.8854},
			{x = 145.8212, y = 2502.7849, z = 16.9057},
			{x = 145.8524, y = 2495.9219, z = 16.8123},
			{x = 145.8861, y = 2488.4873, z = 16.8590},
			{x = 154.0449, y = 2488.0125, z = 16.9363},
			{x = 154.2438, y = 2495.2610, z = 16.9607},
			{x = 154.2361, y = 2502.5022, z = 16.9715},
			{x = 154.2242, y = 2508.7168, z = 16.9385},
			{x = 154.3404, y = 2515.7593, z = 16.9593},
			{x = 164.4704, y = 2515.8196, z = 16.9107},
			{x = 164.6805, y = 2509.2583, z = 16.8401},
			{x = 164.5349, y = 2502.7751, z = 16.8643},
			{x = 164.7446, y = 2495.6880, z = 16.7937},
			{x = 164.6273, y = 2488.1536, z = 16.8579},
			{x = 173.3203, y = 2488.2771, z = 16.9048},
			{x = 173.3849, y = 2495.5183, z = 16.9030},
			{x = 173.4451, y = 2502.2517, z = 16.9452},
			{x = 173.5069, y = 2509.1582, z = 16.9458},
			{x = 173.5656, y = 2515.7273, z = 16.9543},
			{x = 181.7974, y = 2516.2708, z = 16.8329},
			{x = 182.2042, y = 2509.3811, z = 16.8876},
			{x = 181.7170, y = 2502.4299, z = 16.8289},
			{x = 181.8435, y = 2495.2671, z = 16.8561},
			{x = 181.9679, y = 2488.2241, z = 16.8712}
		},
	},
	[2] = {
		farm = {x = 202.4644, y = 2514.5332, z = 16.9963}, 
		barn = {x = 194.4421, y = 2497.9832, z = 16.4974},
		barrel = {x = 202.5349, y = 2490.3657, z = 16.4974},
		spryatalsya = {x = 122.2763, y = 2488.4609, z = 16.5042},
		pits = {
			{x = 215.6530, y = 2487.8848, z = 16.9766},
			{x = 216.1052, y = 2495.5410, z = 16.8490},
			{x = 216.0103, y = 2502.6931, z = 16.8887},
			{x = 215.9713, y = 2509.1956, z = 16.8987},
			{x = 215.9323, y = 2515.6614, z = 16.9189},
			{x = 223.8966, y = 2515.8191, z = 16.9047},
			{x = 223.9501, y = 2509.3533, z = 16.8529},
			{x = 224.0063, y = 2502.7231, z = 16.8581},
			{x = 223.9032, y = 2496.0735, z = 16.7733},
			{x = 223.8372, y = 2488.9541, z = 16.7561},
			{x = 232.1188, y = 2488.1311, z = 16.9445},
			{x = 232.7187, y = 2495.2329, z = 16.8562},
			{x = 232.4960, y = 2502.3037, z = 16.9156},
			{x = 232.6632, y = 2509.0918, z = 16.8659},
			{x = 232.8370, y = 2516.1538, z = 16.8153},
			{x = 242.6354, y = 2515.9321, z = 16.8417},
			{x = 242.2349, y = 2509.5725, z = 16.8477},
			{x = 242.3526, y = 2503.0078, z = 16.8403},
			{x = 242.4469, y = 2495.7007, z = 16.8243},
			{x = 242.5360, y = 2488.7922, z = 16.7620},
			{x = 251.2600, y = 2488.3833, z = 16.8878},
			{x = 251.4253, y = 2495.2112, z = 16.9727},
			{x = 251.3984, y = 2502.3250, z = 16.9634},
			{x = 251.3734, y = 2508.9099, z = 16.9663},
			{x = 251.3475, y = 2515.7568, z = 16.9580},
			{x = 259.3916, y = 2515.9492, z = 16.7873},
			{x = 259.8054, y = 2509.0449, z = 16.8726},
			{x = 260.0284, y = 2502.7839, z = 16.8969},
			{x = 259.9410, y = 2495.4980, z = 16.8809},
			{x = 259.8238, y = 2488.6643, z = 16.8099}
		},
	},
}

local DIALOG_STYLE_MSGBOX = 0
local DIALOG_STYLE_INPUT = 1
local DIALOG_STYLE_LIST = 2
local DIALOG_STYLE_PASSWORD = 3
local DIALOG_STYLE_TABLIST = 4
local DIALOG_STYLE_TABLIST_HEADERS = 5

local farm = 1
local active = false
local blocktp = false
local ignore = false
local ignorestate1 = false
local ignorestate2 = false
local ignorestate3 = false
local ignorestate4 = false
local ignorestate5 = false
local ignorestate6 = false
local ignorestate7 = false
local ignorestate8 = false
local ignorestate9 = false
local ignorestate10 = false
local currentrab = 0
local posad = 1
local updpos = 0
local farm = 1
local sem = 10
local waits = 0
local active123 = false

--[[--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------- Вход в main --------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------]]

function main()
    if not isSampLoaded() or not isSampfuncsLoaded() then return end
	autoupdate("https://gitlab.com/-/snippets/2175425/raw/main/Update.json", '['..string.upper(thisScript().name)..']: ', "https://gitlab.com/soondawnsound/fermpoc/-/raw/main/FermaPoc.lua")
	while not isSampAvailable() do wait(0) end
	sampAddChatMessage("[FermaPoc]: {6CAD0E}Загружено 1.0.8", -1)
	--local vversion = script_version
	--sampAddChatMessage(string.format("{ffffff}[Ferm-Poc] Loaded [Version: %d].", script_version), 0xffffff)
	
	repeat wait(0) until sampIsLocalPlayerSpawned() -- скрипт не будет работать до спавна педа

	sampRegisterChatCommand("fermpoc", function()
		active = not active
		if not active then
			blocktp = false
		end
		sampAddChatMessage(active and "[FermaPoc]: Загружен, для начала работы введите: /vizovbota {ff0000}[Обязательно переоденьтесь!]" or "[FermaPoc]: Oтгружен.", -1)
	end)
	
	sampRegisterChatCommand("vizovbota", function()
		if active then
		    sampAddChatMessage("[FermaPoc]: {6CAD0E}Запущено{ffffff}, для перезагрузки скрипта введите: {AD97EC}/rlbot", -1)
			bot1()
			--thread1:run()
			
		end
	end)
	
	sampRegisterChatCommand("checkst", function()
        local ssem = sem -- по иду получаем ник и записываем его в переменную nick
		sampAddChatMessage(string.format("{ffffff}[Ferm-Poc] [ST ID: %d].", ssem), 0xffffff)
	end)
	
	sampRegisterChatCommand("changest", show_menu)
	
	sampRegisterChatCommand("helpfermer", function()
	    sampShowDialog(111, "FermPoc Helper", "{ffffff}/fermpoc - подгрузить систему.\n/vizovbota - запуск системы {ff0000}(обязательно после подгрузки!)\n{ffffff}/helpfermer - команды.\n/reloadfermer - запустить работу сначала\n/reloadfermer_2 - запустить уже готовые фермы ворк\n/rlbot - перезагрузить скрипт полностью\n/changeferm1 - выбрать 3-ую ферму\n/changeferm2 - выбрать 2-ую ферму", "Закрыть", DIALOG_STYLE_MSGBOX)
	end)

	sampRegisterChatCommand("reloadfermer", function() -- beta
	    if active then
            reloadedbot()
	    end
	end)
	
	sampRegisterChatCommand("reloadfermer_2", function() -- beta
	    if active then
            reloadedbot2()
	    end
	end)
	
	sampRegisterChatCommand("changeferm1", function()
     lua_thread.create(function()
	    wait(500)
        farm = 1
		wait(500)
		--setCharCoordinates(PLAYER_PED, farms[farm].farm.x, farms[farm].farm.y, farms[farm].farm.z)
		return true
	    end)   
	end)
	sampRegisterChatCommand("changeferm2", function()
     lua_thread.create(function()
	    wait(500)
        farm = 2
		wait(500)
		--setCharCoordinates(PLAYER_PED, farms[farm].farm.x, farms[farm].farm.y, farms[farm].farm.z)
		return true
	    end)   
	end)
	
	sampRegisterChatCommand("rlbot", function()
		if active then
		    sampAddChatMessage("[FermaPoc]: {FF0000}Reloaded!{ffffff}", -1)
			thisScript():reload()
		else
		sampAddChatMessage("[FermaPoc]: Статус бота: не активен. Перезагрузка не удалась, используйте CTRL + R.", -1)
		end
	
	end)

    sampRegisterChatCommand("zakupkatrav", test_func)
	
--[[	while true do
	   local result, button, list, input = sampHasDialogRespond(107)
    if result then -- если диалог активен...
      if button == 1 and list == 0 then -- если нажата кнопка 1 и строка равна 0 то...
        sampAddChatMessage("строка0", -1) end -- выводим "строка0" локально
      if button == 1 and list == 1 then -- если нажата кнопка 1 и строка равна 1 то...
        sampAddChatMessage("строка1", -1) end -- выводим "строка1" локально
      if button == 1 and list == 2 then -- если нажата кнопка 1 и строка равна 2 то...
        sampAddChatMessage("строка2", -1) end -- выводим "строка2" локально
	  if button == 1 and list == 3 then -- если нажата кнопка 1 и строка равна 2 то...
        sampAddChatMessage("строка3", -1) end -- выводим "строка2" локально
		if button == 1 and list == 4 then -- если нажата кнопка 1 и строка равна 2 то...
        sampAddChatMessage("строка4", -1) end -- выводим "строка2" локально
		if button == 1 and list == 5 then -- если нажата кнопка 1 и строка равна 2 то...
        sampAddChatMessage("строка5", -1) end -- выводим "строка2" локально
		if button == 1 and list == 6 then -- если нажата кнопка 1 и строка равна 2 то...
        sampAddChatMessage("строка6", -1) end -- выводим "строка2" локально
		if button == 1 and list == 7 then -- если нажата кнопка 1 и строка равна 2 то...
        sampAddChatMessage("строка7", -1) end -- выводим "строка2" локально
		if button == 1 and list == 8 then -- если нажата кнопка 1 и строка равна 2 то...
        sampAddChatMessage("строка8", -1) end -- выводим "строка2" локально
		if button == 1 and list == 9 then -- если нажата кнопка 1 и строка равна 2 то...
        sampAddChatMessage("строка9", -1) end -- выводим "строка2" локально
		if button == 1 and list == 10 then -- если нажата кнопка 1 и строка равна 2 то...
        sampAddChatMessage("строка10", -1) end -- выводим "строка2" локально
		if button == 1 and list == 11 then -- если нажата кнопка 1 и строка равна 2 то...
        sampAddChatMessage("строка11", -1) end -- выводим "строка2" локально
		if button == 1 and list == 12 then -- если нажата кнопка 1 и строка равна 2 то...
        sampAddChatMessage("строка12", -1) end -- выводим "строка2" локально
		if button == 1 and list == 13 then -- если нажата кнопка 1 и строка равна 2 то...
        sampAddChatMessage("строка13", -1) end -- выводим "строка2" локально
		if button == 1 and list == 14 then -- если нажата кнопка 1 и строка равна 2 то...
        sampAddChatMessage("строка14", -1) end -- выводим "строка2" локально
		if button == 1 and list == 15 then -- если нажата кнопка 1 и строка равна 2 то...
		sampAddChatMessage("строка15", -1) end -- выводим "строка2" локально
        end
    end]]
end
--[[--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------- Важная функция для работы скрипта  --------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------]]

function checkAkk() -- чекаем участок
	if isCharInArea2d(PLAYER_PED, 84.2025, 2464.6182, 201.6283, 2538.4719, false) then
		return true
	else
		return false
	end
end

function blockRabota()
	lua_thread.create(function()
		local checkhour = os.date("%H") + 1
		if currentrab ~= 0 then
			if currentrab == 1 then
				ignorestate1 = true
			elseif currentrab == 2 then
				ignorestate2 = true
			elseif currentrab == 3 then
				ignorestate3 = true
			elseif currentrab == 4 then
				ignorestate4 = true
			elseif currentrab == 5 then
				ignorestate5 = true
			elseif currentrab == 6 then
				ignorestate6 = true
			elseif currentrab == 7 then
				ignorestate7 = true
			elseif currentrab == 8 then
				ignorestate8 = true
			elseif currentrab == 9 then
				ignorestate9 = true
			elseif currentrab == 10 then
				ignorestate10 = true
			end
		end
		while true do
			wait(0)
			if os.date("%H") == checkhour then
				ignorestate1 = false
				ignorestate2 = false
				ignorestate3 = false
				ignorestate4 = false
				ignorestate5 = false
				ignorestate6 = false
				ignorestate7 = false
				ignorestate8 = false
				ignorestate9 = false
				ignorestate10 = false
				break
			end
		end
	end)
end

function bot()
        lua_thread.create(function()
--		goToBoting()
--		wait(20000)
        sampAddChatMessage(string.format("[Ferm-Poc 2] Начинается сборка урожая. [Farm ID: %d].", ffarm), 0xffffff)
        wait(400)
        ssborshik()		
        wait(495000)
        print("[Ferm-Poc] Начинается 1-ый этап бота")
		sampAddChatMessage(string.format("[Ferm-Poc 2] Начинается выкопка. [Farm ID: %d].", ffarm), 0xffffff)
		wait(200)
--		workvikopka()
		wait(200000)
        print("[Ferm-Poc] Выкопали")
		sampAddChatMessage(string.format("[Ferm-Poc 2] Выкопали. [Farm ID: %d].", ffarm), 0xffffff)
		-- логи всякие
        wait(200)
--		worksadka()
		wait(397000)
        print("[Ferm-Poc] Посадили")
		sampAddChatMessage(string.format("[Ferm-Poc 2] Посадили. [Farm ID: %d].", ffarm), 0xffffff)
		--
        wait(200)
--		workpoliv()
		wait(296000)
        print("[Ferm-Poc] Полили")
		sampAddChatMessage(string.format("[Ferm-Poc 2] Полили. [Farm ID: %d].", ffarm), 0xffffff)
		---
        wait(200)
--		workpolka()
		wait(190000)
		print("[Ferm-Poc] Пропололи, переключаемся на соседнюю ферму.")
		sampAddChatMessage(string.format("[Ferm-Poc 2] Прополили. [Farm ID: %d].", ffarm), 0xffffff)
		--wait(waits)
		wait(1000)
		--wait(100)
		ChangeNaTwo()
		wait(7500)
		--wait(10)
		return
    end)
	--return
end

function bot1()
        lua_thread.create(function()
		local ffarm = farm
        wait(1000)
        print("[Ferm-Poc 1] Начинается выкопка")
		sampAddChatMessage(string.format("[Ferm-Poc 1] Начинается выкопка. [Farm ID: %d].", ffarm), 0xffffff)
		wait(200)
		workvikopka()
		wait(200000)
        print("[Ferm-Poc 1] Выкопали")
		sampAddChatMessage(string.format("[Ferm-Poc 1] Выкопали. [Farm ID: %d].", ffarm), 0xffffff)
		-- логи всякие
        wait(200)
		worksadka()
		wait(397000)
        print("[Ferm-Poc 1] Посадили")
		sampAddChatMessage(string.format("[Ferm-Poc 1] Посадили. [Farm ID: %d].", ffarm), 0xffffff)
		--
        wait(200)
		workpoliv()
		wait(296000)
        print("[Ferm-Poc 1] Полили")
		sampAddChatMessage(string.format("[Ferm-Poc 1] Полили. [Farm ID: %d].", ffarm), 0xffffff)
		---
        wait(200)
		workpolka()
		wait(190000)
		print("[Ferm-Poc 1] Пропололи, переключаемся на соседнюю ферму.")
		sampAddChatMessage(string.format("[Ferm-Poc 1] Прополили. [Farm ID: %d].", ffarm), 0xffffff)
		wait(1000)
		wait(100)
		
		ChangeNaTwo()
		
		wait(6000)
		wait(1000)
        print("[Ferm-Poc 1] Начинается выкопка")
		sampAddChatMessage(string.format("[Ferm-Poc 1] Начинается выкопка. [Farm ID: %d].", ffarm), 0xffffff)
		wait(200)
		workvikopka()
		wait(200000)
        print("[Ferm-Poc 1] Выкопали")
		sampAddChatMessage(string.format("[Ferm-Poc 1] Выкопали. [Farm ID: %d].", ffarm), 0xffffff)
		--логи всякие
        wait(200)
		worksadka()
		wait(397000)
        print("[Ferm-Poc 1] Посадили")
		sampAddChatMessage(string.format("[Ferm-Poc 1] Посадили. [Farm ID: %d].", ffarm), 0xffffff)
		--
        wait(200)
		workpoliv()
		wait(296000)
        print("[Ferm-Poc 1] Полили")
		sampAddChatMessage(string.format("[Ferm-Poc 1] Полили. [Farm ID: %d].", ffarm), 0xffffff)
		---
        wait(200)
		workpolka()
		wait(190000)
		print("[Ferm-Poc 1] Пропололи, переключаемся на соседнюю ферму.")
		sampAddChatMessage(string.format("[Ferm-Poc 1] Прополили. [Farm ID: %d].", ffarm), 0xffffff)
		wait(1000)
		wait(100)
		ChangeNaThree1()
		wait(6000)
		return
    end)
	--return
end
-- Основные действия
function workpolka() -- 190000
    lua_thread.create(function()
		vgrabli()
		wait(1800)
		prop(farms[farm].pits[1].x, farms[farm].pits[1].y, farms[farm].pits[1].z)
		wait(6200)
		prop(farms[farm].pits[2].x, farms[farm].pits[2].y, farms[farm].pits[2].z)
		wait(6200)
		prop(farms[farm].pits[3].x, farms[farm].pits[3].y, farms[farm].pits[3].z)
		wait(6200)
		prop(farms[farm].pits[4].x, farms[farm].pits[4].y, farms[farm].pits[4].z)
		wait(6200)
		prop(farms[farm].pits[5].x, farms[farm].pits[5].y, farms[farm].pits[5].z)
		wait(6200)
		prop(farms[farm].pits[6].x, farms[farm].pits[6].y, farms[farm].pits[6].z)
		wait(6200)
		prop(farms[farm].pits[7].x, farms[farm].pits[7].y, farms[farm].pits[7].z)
		wait(6200)
		prop(farms[farm].pits[8].x, farms[farm].pits[8].y, farms[farm].pits[8].z)
		wait(6200)
		prop(farms[farm].pits[9].x, farms[farm].pits[9].y, farms[farm].pits[9].z)
		wait(6200)
		prop(farms[farm].pits[10].x, farms[farm].pits[10].y, farms[farm].pits[10].z)
		wait(6200)
		prop(farms[farm].pits[11].x, farms[farm].pits[11].y, farms[farm].pits[11].z)
		wait(6200)
		prop(farms[farm].pits[12].x, farms[farm].pits[12].y, farms[farm].pits[12].z)
		wait(6200)
		prop(farms[farm].pits[13].x, farms[farm].pits[13].y, farms[farm].pits[13].z)
		wait(6200)
		prop(farms[farm].pits[14].x, farms[farm].pits[14].y, farms[farm].pits[14].z)
		wait(6200)
		prop(farms[farm].pits[15].x, farms[farm].pits[15].y, farms[farm].pits[15].z)
		wait(6200)
		prop(farms[farm].pits[16].x, farms[farm].pits[16].y, farms[farm].pits[16].z)
		wait(6200)
		prop(farms[farm].pits[17].x, farms[farm].pits[17].y, farms[farm].pits[17].z)
		wait(6200)
		prop(farms[farm].pits[18].x, farms[farm].pits[18].y, farms[farm].pits[18].z)
		wait(6200)
		prop(farms[farm].pits[19].x, farms[farm].pits[19].y, farms[farm].pits[19].z)
		wait(6200)
		prop(farms[farm].pits[20].x, farms[farm].pits[20].y, farms[farm].pits[20].z)
		wait(6200)
		prop(farms[farm].pits[21].x, farms[farm].pits[21].y, farms[farm].pits[21].z)
		wait(6200)
		prop(farms[farm].pits[22].x, farms[farm].pits[22].y, farms[farm].pits[22].z)
		wait(6200)
		prop(farms[farm].pits[23].x, farms[farm].pits[23].y, farms[farm].pits[23].z)
		wait(6200)
		prop(farms[farm].pits[24].x, farms[farm].pits[24].y, farms[farm].pits[24].z)
		wait(6200)
		prop(farms[farm].pits[25].x, farms[farm].pits[25].y, farms[farm].pits[25].z)
		wait(6200)
		prop(farms[farm].pits[26].x, farms[farm].pits[26].y, farms[farm].pits[26].z)
		wait(6200)
		prop(farms[farm].pits[27].x, farms[farm].pits[27].y, farms[farm].pits[27].z)
		wait(6200)
		prop(farms[farm].pits[28].x, farms[farm].pits[28].y, farms[farm].pits[28].z)
		wait(6200)
		prop(farms[farm].pits[29].x, farms[farm].pits[29].y, farms[farm].pits[29].z)
		wait(6200)
		prop(farms[farm].pits[30].x, farms[farm].pits[30].y, farms[farm].pits[30].z)
		wait(6200)
		pgrabli()
		wait(1800) -- 189.6
		setCharCoordinates(PLAYER_PED, farms[farm].spryatalsya.x, farms[farm].spryatalsya.y, farms[farm].spryatalsya.z) -- тп в амбар
	end)
end

function workpoliv() -- 296000
    lua_thread.create(function()
		wait(500)
		vvedro()
		wait(1800)
		poliv(farms[farm].pits[1].x, farms[farm].pits[1].y, farms[farm].pits[1].z)
		wait(9700)
		poliv(farms[farm].pits[2].x, farms[farm].pits[2].y, farms[farm].pits[2].z)
		wait(9700)
		poliv(farms[farm].pits[3].x, farms[farm].pits[3].y, farms[farm].pits[3].z)
		wait(9700)
		poliv(farms[farm].pits[4].x, farms[farm].pits[4].y, farms[farm].pits[4].z)
		wait(9700)
		poliv(farms[farm].pits[5].x, farms[farm].pits[5].y, farms[farm].pits[5].z)
		wait(9700)
		poliv(farms[farm].pits[6].x, farms[farm].pits[6].y, farms[farm].pits[6].z)
		wait(9700)
		poliv(farms[farm].pits[7].x, farms[farm].pits[7].y, farms[farm].pits[7].z)
		wait(9700)
		poliv(farms[farm].pits[8].x, farms[farm].pits[8].y, farms[farm].pits[8].z)
		wait(9700)
		poliv(farms[farm].pits[9].x, farms[farm].pits[9].y, farms[farm].pits[9].z)
		wait(9700)
		poliv(farms[farm].pits[10].x, farms[farm].pits[10].y, farms[farm].pits[10].z)
		wait(9700)
		poliv(farms[farm].pits[11].x, farms[farm].pits[11].y, farms[farm].pits[11].z)
		wait(9700)
		poliv(farms[farm].pits[12].x, farms[farm].pits[12].y, farms[farm].pits[12].z)
		wait(9700)
		poliv(farms[farm].pits[13].x, farms[farm].pits[13].y, farms[farm].pits[13].z)
		wait(9700)
		poliv(farms[farm].pits[14].x, farms[farm].pits[14].y, farms[farm].pits[14].z)
		wait(9700)
		poliv(farms[farm].pits[15].x, farms[farm].pits[15].y, farms[farm].pits[15].z)
		wait(9700)
		poliv(farms[farm].pits[16].x, farms[farm].pits[16].y, farms[farm].pits[16].z)
		wait(9700)
		poliv(farms[farm].pits[17].x, farms[farm].pits[17].y, farms[farm].pits[17].z)
		wait(9700)
		poliv(farms[farm].pits[18].x, farms[farm].pits[18].y, farms[farm].pits[18].z)
		wait(9700)
		poliv(farms[farm].pits[19].x, farms[farm].pits[19].y, farms[farm].pits[19].z)
		wait(9700)
		poliv(farms[farm].pits[20].x, farms[farm].pits[20].y, farms[farm].pits[20].z)
		wait(9700)
		poliv(farms[farm].pits[21].x, farms[farm].pits[21].y, farms[farm].pits[21].z)
		wait(9700)
		poliv(farms[farm].pits[22].x, farms[farm].pits[22].y, farms[farm].pits[22].z)
		wait(9700)
		poliv(farms[farm].pits[23].x, farms[farm].pits[23].y, farms[farm].pits[23].z)
		wait(9700)
		poliv(farms[farm].pits[24].x, farms[farm].pits[24].y, farms[farm].pits[24].z)
		wait(9700)
		poliv(farms[farm].pits[25].x, farms[farm].pits[25].y, farms[farm].pits[25].z)
		wait(9700)
		poliv(farms[farm].pits[26].x, farms[farm].pits[26].y, farms[farm].pits[26].z)
		wait(9700)
		poliv(farms[farm].pits[27].x, farms[farm].pits[27].y, farms[farm].pits[27].z)
		wait(9700)
		poliv(farms[farm].pits[28].x, farms[farm].pits[28].y, farms[farm].pits[28].z)
		wait(9700)
		poliv(farms[farm].pits[29].x, farms[farm].pits[29].y, farms[farm].pits[29].z)
		wait(9700)
		poliv(farms[farm].pits[30].x, farms[farm].pits[30].y, farms[farm].pits[30].z)
		wait(9700)
		pvedro()
		wait(1800) -- 295500
	end)
end

function worksadka() -- садка урожая - 397000
	lua_thread.create(function()
		wait(500)
		semen(farms[farm].pits[1].x, farms[farm].pits[1].y, farms[farm].pits[1].z)
		wait(13200)
		semen(farms[farm].pits[2].x, farms[farm].pits[2].y, farms[farm].pits[2].z)
		wait(13200)
		semen(farms[farm].pits[3].x, farms[farm].pits[3].y, farms[farm].pits[3].z)
		wait(13200)
		semen(farms[farm].pits[4].x, farms[farm].pits[4].y, farms[farm].pits[4].z)
		wait(13200)
		semen(farms[farm].pits[5].x, farms[farm].pits[5].y, farms[farm].pits[5].z)
		wait(13200)
		semen(farms[farm].pits[6].x, farms[farm].pits[6].y, farms[farm].pits[6].z)
		wait(13200)
		semen(farms[farm].pits[7].x, farms[farm].pits[7].y, farms[farm].pits[7].z)
		wait(13200)
		semen(farms[farm].pits[8].x, farms[farm].pits[8].y, farms[farm].pits[8].z)
		wait(13200)
		semen(farms[farm].pits[9].x, farms[farm].pits[9].y, farms[farm].pits[9].z)
		wait(13200)
		semen(farms[farm].pits[10].x, farms[farm].pits[10].y, farms[farm].pits[10].z)
		wait(13200)
		semen(farms[farm].pits[11].x, farms[farm].pits[11].y, farms[farm].pits[11].z)
		wait(13200)
		semen(farms[farm].pits[12].x, farms[farm].pits[12].y, farms[farm].pits[12].z)
		wait(13200)
		semen(farms[farm].pits[13].x, farms[farm].pits[13].y, farms[farm].pits[13].z)
		wait(13200)
		semen(farms[farm].pits[14].x, farms[farm].pits[14].y, farms[farm].pits[14].z)
		wait(13200)
		semen(farms[farm].pits[15].x, farms[farm].pits[15].y, farms[farm].pits[15].z)
		wait(13200)
		semen(farms[farm].pits[16].x, farms[farm].pits[16].y, farms[farm].pits[16].z)
		wait(13200)
		semen(farms[farm].pits[17].x, farms[farm].pits[17].y, farms[farm].pits[17].z)
		wait(13200)
		semen(farms[farm].pits[18].x, farms[farm].pits[18].y, farms[farm].pits[18].z)
		wait(13200)
		semen(farms[farm].pits[19].x, farms[farm].pits[19].y, farms[farm].pits[19].z)
		wait(13200)
		semen(farms[farm].pits[20].x, farms[farm].pits[20].y, farms[farm].pits[20].z)
		wait(13200)
		semen(farms[farm].pits[21].x, farms[farm].pits[21].y, farms[farm].pits[21].z)
		wait(13200)
		semen(farms[farm].pits[22].x, farms[farm].pits[22].y, farms[farm].pits[22].z)
		wait(13200)
		semen(farms[farm].pits[23].x, farms[farm].pits[23].y, farms[farm].pits[23].z)
		wait(13200)
		semen(farms[farm].pits[24].x, farms[farm].pits[24].y, farms[farm].pits[24].z)
		wait(13200)
		semen(farms[farm].pits[25].x, farms[farm].pits[25].y, farms[farm].pits[25].z)
		wait(13200)
		semen(farms[farm].pits[26].x, farms[farm].pits[26].y, farms[farm].pits[26].z)
		wait(13200)
		semen(farms[farm].pits[27].x, farms[farm].pits[27].y, farms[farm].pits[27].z)
		wait(13200)
		semen(farms[farm].pits[28].x, farms[farm].pits[28].y, farms[farm].pits[28].z)
		wait(13200)
		semen(farms[farm].pits[29].x, farms[farm].pits[29].y, farms[farm].pits[29].z)
		wait(13200)
		semen(farms[farm].pits[30].x, farms[farm].pits[30].y, farms[farm].pits[30].z)
		wait(13200) -- 396800
	end)
end

function workvikopka() -- выкопка ямы -- 200000
	lua_thread.create(function()
		wait(500)
	    vlopata()
		wait(1800)
	    vikop(farms[farm].pits[1].x, farms[farm].pits[1].y, farms[farm].pits[1].z)
		wait(6500)
		vikop(farms[farm].pits[2].x, farms[farm].pits[2].y, farms[farm].pits[2].z)
		wait(6500)
		vikop(farms[farm].pits[3].x, farms[farm].pits[3].y, farms[farm].pits[3].z)
		wait(6500)
		vikop(farms[farm].pits[4].x, farms[farm].pits[4].y, farms[farm].pits[4].z)
		wait(6500)
		vikop(farms[farm].pits[5].x, farms[farm].pits[5].y, farms[farm].pits[5].z)
		wait(6500)
		vikop(farms[farm].pits[6].x, farms[farm].pits[6].y, farms[farm].pits[6].z)
		wait(6500)
		vikop(farms[farm].pits[7].x, farms[farm].pits[7].y, farms[farm].pits[7].z)
		wait(6500)
		vikop(farms[farm].pits[8].x, farms[farm].pits[8].y, farms[farm].pits[8].z)
		wait(6500)
		vikop(farms[farm].pits[9].x, farms[farm].pits[9].y, farms[farm].pits[9].z)
		wait(6500)
		vikop(farms[farm].pits[10].x, farms[farm].pits[10].y, farms[farm].pits[10].z)
		wait(6500)
		vikop(farms[farm].pits[11].x, farms[farm].pits[11].y, farms[farm].pits[11].z)
		wait(6500)
		vikop(farms[farm].pits[12].x, farms[farm].pits[12].y, farms[farm].pits[12].z)
		wait(6500)
		vikop(farms[farm].pits[13].x, farms[farm].pits[13].y, farms[farm].pits[13].z)
		wait(6500)
		vikop(farms[farm].pits[14].x, farms[farm].pits[14].y, farms[farm].pits[14].z)
		wait(6500)
		vikop(farms[farm].pits[15].x, farms[farm].pits[15].y, farms[farm].pits[15].z)
		wait(6500)
		vikop(farms[farm].pits[16].x, farms[farm].pits[16].y, farms[farm].pits[16].z)
		wait(6500)
		vikop(farms[farm].pits[17].x, farms[farm].pits[17].y, farms[farm].pits[17].z)
		wait(6500)
		vikop(farms[farm].pits[18].x, farms[farm].pits[18].y, farms[farm].pits[18].z)
		wait(6500)
		vikop(farms[farm].pits[19].x, farms[farm].pits[19].y, farms[farm].pits[19].z)
		wait(6500)
		vikop(farms[farm].pits[20].x, farms[farm].pits[20].y, farms[farm].pits[20].z)
		wait(6500)
		vikop(farms[farm].pits[21].x, farms[farm].pits[21].y, farms[farm].pits[21].z)
		wait(6500)
		vikop(farms[farm].pits[22].x, farms[farm].pits[22].y, farms[farm].pits[22].z)
		wait(6500)
		vikop(farms[farm].pits[23].x, farms[farm].pits[23].y, farms[farm].pits[23].z)
		wait(6500)
		vikop(farms[farm].pits[24].x, farms[farm].pits[24].y, farms[farm].pits[24].z)
		wait(6500)
		vikop(farms[farm].pits[25].x, farms[farm].pits[25].y, farms[farm].pits[25].z)
        wait(6500)
		vikop(farms[farm].pits[26].x, farms[farm].pits[26].y, farms[farm].pits[26].z)
		wait(6500)
		vikop(farms[farm].pits[27].x, farms[farm].pits[27].y, farms[farm].pits[27].z)
		wait(6500)
		vikop(farms[farm].pits[28].x, farms[farm].pits[28].y, farms[farm].pits[28].z)
		wait(6500)
		vikop(farms[farm].pits[29].x, farms[farm].pits[29].y, farms[farm].pits[29].z)
		wait(6500)
		vikop(farms[farm].pits[30].x, farms[farm].pits[30].y, farms[farm].pits[30].z)
		wait(6500)
		plopata()
		wait(1800) -- 199500
	end)
end

function ssborshik() -- сборка урожая -- 495000
	lua_thread.create(function()
		sbor(farms[farm].pits[1].x, farms[farm].pits[1].y, farms[farm].pits[1].z)
		wait(14500)
		sbor(farms[farm].pits[2].x, farms[farm].pits[2].y, farms[farm].pits[2].z)
		wait(14500)
		sbor(farms[farm].pits[3].x, farms[farm].pits[3].y, farms[farm].pits[3].z)
		wait(14500)
		sbor(farms[farm].pits[4].x, farms[farm].pits[4].y, farms[farm].pits[4].z)
		wait(14500)
		sbor(farms[farm].pits[5].x, farms[farm].pits[5].y, farms[farm].pits[5].z)
		wait(14500)
		sbor(farms[farm].pits[6].x, farms[farm].pits[6].y, farms[farm].pits[6].z)
		wait(14500)
		sbor(farms[farm].pits[7].x, farms[farm].pits[7].y, farms[farm].pits[7].z)
		wait(14500)
		sbor(farms[farm].pits[8].x, farms[farm].pits[8].y, farms[farm].pits[8].z)
		wait(14500)
		sbor(farms[farm].pits[9].x, farms[farm].pits[9].y, farms[farm].pits[9].z)
		wait(14500)
		sbor(farms[farm].pits[10].x, farms[farm].pits[10].y, farms[farm].pits[10].z)
		wait(14500)
		sbor(farms[farm].pits[11].x, farms[farm].pits[11].y, farms[farm].pits[11].z)
		wait(14500)
		sbor(farms[farm].pits[12].x, farms[farm].pits[12].y, farms[farm].pits[12].z)
		wait(14500)
		sbor(farms[farm].pits[13].x, farms[farm].pits[13].y, farms[farm].pits[13].z)
		wait(14500)
		sbor(farms[farm].pits[14].x, farms[farm].pits[14].y, farms[farm].pits[14].z)
		wait(14500)
		sbor(farms[farm].pits[15].x, farms[farm].pits[15].y, farms[farm].pits[15].z)
		wait(14500)
		sbor(farms[farm].pits[16].x, farms[farm].pits[16].y, farms[farm].pits[16].z)
		wait(14500)
		sbor(farms[farm].pits[17].x, farms[farm].pits[17].y, farms[farm].pits[17].z)
		wait(14500)
		sbor(farms[farm].pits[18].x, farms[farm].pits[18].y, farms[farm].pits[18].z)
		wait(14500)
		sbor(farms[farm].pits[19].x, farms[farm].pits[19].y, farms[farm].pits[19].z)
		wait(14500)
		sbor(farms[farm].pits[20].x, farms[farm].pits[20].y, farms[farm].pits[20].z)
		wait(14500)
		sbor(farms[farm].pits[21].x, farms[farm].pits[21].y, farms[farm].pits[21].z)
		wait(14500)
		sbor(farms[farm].pits[22].x, farms[farm].pits[22].y, farms[farm].pits[22].z)
		wait(14500)
		sbor(farms[farm].pits[23].x, farms[farm].pits[23].y, farms[farm].pits[23].z)
		wait(14500)
		sbor(farms[farm].pits[24].x, farms[farm].pits[24].y, farms[farm].pits[24].z)
		wait(14500)
		sbor(farms[farm].pits[25].x, farms[farm].pits[25].y, farms[farm].pits[25].z)
		wait(14500)
		sbor(farms[farm].pits[26].x, farms[farm].pits[26].y, farms[farm].pits[26].z)
		wait(14500)
		sbor(farms[farm].pits[27].x, farms[farm].pits[27].y, farms[farm].pits[27].z)
		wait(14500)
		sbor(farms[farm].pits[28].x, farms[farm].pits[28].y, farms[farm].pits[28].z)
		wait(14500)
		sbor(farms[farm].pits[29].x, farms[farm].pits[29].y, farms[farm].pits[29].z)
		wait(14500)
		sbor(farms[farm].pits[30].x, farms[farm].pits[30].y, farms[farm].pits[30].z)
		wait(14500) -- 495000
	end)
end

function ChangeNaTwo()
    lua_thread.create(function()
	    --lua_thread:terminate(botik)
		local ffarm = farm -- по иду получаем ник и записываем его в переменную nick
	    print("[Ferm-Poc] Переключаемся на соседнюю ферму.")
		sampAddChatMessage(string.format("{ffffff}[Ferm-Poc] Преключаемся на соседнюю ферму [Farm ID: %d].", ffarm), 0xffffff)
	    wait(300)
	    active = false
		wait(300)
	    goToBoting3()
		wait(4000)
		farm = 2
		wait(500)
		goToBoting2()
		wait(400)
		active = true
		wait(300)
--		bot()
--		ChangeNaThree1()
		sampAddChatMessage(string.format("[Ferm-Poc] Изменено. [Farm ID: %d].", ffarm), 0xffffff)
		return
	end)
end

function ChangeNaThree()
    lua_thread.create(function()
    	local ffarm = farm -- по иду получаем ник и записываем его в переменную nick
	    print("[Ferm-Poc] Переключаемся на соседнюю ферму.")
		sampAddChatMessage(string.format("{ffffff}[Ferm-Poc] Преключаемся на соседнюю ферму [Farm ID: %d].", ffarm), 0xffffff)
	    wait(200)
	    active = false
		wait(200)
	    goToBoting2()
		wait(4000)
		farm = 1
		wait(500)
		goToBoting3()
		wait(4000)
		active = true
		wait(200)
		sampAddChatMessage(string.format("[Ferm-Poc] Изменено. [Farm ID: %d].", ffarm), 0xffffff)
		return
		--bot1()
		--bot()
	end)
end

function ChangeNaTwo1()
    lua_thread.create(function()
	    --lua_thread:terminate(botik)
		local ffarm = farm -- по иду получаем ник и записываем его в переменную nick
	    print("[Ferm-Poc] Переключаемся на соседнюю ферму.")
		sampAddChatMessage(string.format("{ffffff}[Ferm-Poc] Преключаемся на соседнюю ферму [Farm ID: %d].", ffarm), 0xffffff)
	    wait(300)
	    active = false
		wait(300)
	    goToBoting3()
		wait(4000)
		farm = 2
		wait(500)
		goToBoting2()
		wait(400)
		active = true
		wait(300)
		bot()
--		ChangeNaThree()
		sampAddChatMessage(string.format("[Ferm-Poc] Изменено. [Farm ID: %d].", ffarm), 0xffffff)
		return
	end)
end

function ChangeNaThree1()
    lua_thread.create(function()
    	local ffarm = farm -- по иду получаем ник и записываем его в переменную nick
	    print("[Ferm-Poc] Переключаемся на соседнюю ферму.")
		sampAddChatMessage(string.format("{ffffff}[Ferm-Poc] Преключаемся на соседнюю ферму [Farm ID: %d].", ffarm), 0xffffff)
	    wait(200)
	    active = false
		wait(200)
	    goToBoting2()
		wait(4000)
		farm = 1
		wait(500)
		goToBoting3()
		wait(4000)
		active = true
		wait(200)
		sampAddChatMessage(string.format("[Ferm-Poc] Изменено. [Farm ID: %d].", ffarm), 0xffffff)
		bot()
		return
		--bot1()
	end)
end

-- Основные действия

-- Переносные действия
function poliv(x, y, z) -- полив - 9300
	lua_thread.create(function()
		wait(200) -- ждём
		setCharCoordinates(PLAYER_PED, farms[farm].barrel.x, farms[farm].barrel.y, farms[farm].barrel.z) -- тпаемся к бочке с водой
		wait(200) -- ждём
		pressAlt()
		wait(5400) -- ждём
		setCharCoordinates(PLAYER_PED, x, y, z) -- тпаемся к урожаю
		wait(200)
		pressAlt()
		wait(3300) -- ждём
		sampCloseCurrentDialogWithButton(0) -- закрываем диалог после цикла
		blocktp = false -- включаем проверку на новые действия
		currentrab = 0
	end)
end

function sbor(x, y, z) -- сбор - 14200
	lua_thread.create(function()
		wait(200) -- ждём
		setCharCoordinates(PLAYER_PED, x, y, z) -- тпаемся к урожаю
		wait(200) -- ждём
		pressAlt()
		wait(13500) -- ждём
		setCharCoordinates(PLAYER_PED, farms[farm].barn.x, farms[farm].barn.y, farms[farm].barn.z) -- тпаемся к амбару для сдачи
		wait(300) -- ждём
		pressAlt()
		sampCloseCurrentDialogWithButton(0) -- закрываем диалог после цикла
		blocktp = false -- включаем проверку на новые действия
		currentrab = 0
	end)
end

function prop(x, y, z) -- прополка -- 5700
	lua_thread.create(function()
		wait(200) -- ждём
		setCharCoordinates(PLAYER_PED, x, y, z) -- тпаемся к урожаю
		wait(200) -- ждём
		pressAlt()
		wait(5300)
		sampCloseCurrentDialogWithButton(0) -- закрываем диалог после цикла
		blocktp = false -- включаем проверку на новые действия
		currentrab = 0
	end)
end

function vikop(x, y, z) -- копаем яму - 6000
	lua_thread.create(function()
		wait(100) -- ждём
		setCharCoordinates(PLAYER_PED, x, y, z) -- тпаемся к урожаю
		wait(200) -- ждём
		pressAlt()
		wait(200) -- ждём
		--sampSendDialogResponse(sampGetCurrentDialogId(), 1, posad, nil) -- выбираем что посадить
		sampSendDialogResponse(sampGetCurrentDialogId(), 1, 1, nil) -- выбираем что посадить
		wait(200)
		sampCloseCurrentDialogWithButton(0) -- закрываем диалог после цикла
		wait(5300) -- ждём
		blocktp = false -- включаем проверку на новые действия
		currentrab = 0
	end)
end

function semen(x, y, z, sem) -- садим семена - 12800
	lua_thread.create(function()
		setCharCoordinates(PLAYER_PED, farms[farm].barn.x, farms[farm].barn.y, farms[farm].barn.z) -- тп к амбару
		wait(200) -- задержка после тп
		pressAlt()
		wait(200) -- ждём
		sampSendDialogResponse(sampGetCurrentDialogId(), 1, 0, nil) -- нажимаем на нулевой пункт диалога
		wait(200) -- ждём
		--sampSendDialogResponse(sampGetCurrentDialogId(), 1, sem, nil) -- нажимаем на пункт (переменная)
		sampSendDialogResponse(sampGetCurrentDialogId(), 1, 10, nil) -- нажимаем на пункт (переменная)
		wait(200)
		sampSendDialogResponse(sampGetCurrentDialogId(), 1, 0, nil) -- нажимаем на нулевой пункт диалога
		wait(100) -- ждём
		sampCloseCurrentDialogWithButton(0) -- закрываем диалог после цикла
		wait(200)
		setCharCoordinates(PLAYER_PED, x, y, z) -- тпаемся к урожаю
		wait(200) -- ждём
		pressAlt()
		wait(11500) -- ждём сбора урожая
--		setCharCoordinates(PLAYER_PED, farms[farm].barn.x, farms[farm].barn.y, farms[farm].barn.z) -- тпаемся к амбару для сдачи
--		wait(500) -- ждём
--		pressAlt()
		sampCloseCurrentDialogWithButton(0) -- закрываем диалог после цикла
		blocktp = false -- включаем проверку на новые действия
		currentrab = 0
	end)
end

function plopata() -- вернуть лопату
	lua_thread.create(function()
	    wait(200)
		setCharCoordinates(PLAYER_PED, farms[farm].barn.x, farms[farm].barn.y, farms[farm].barn.z) -- тп к амбару
		wait(250) -- задержка после тп
        pressAlt()
		wait(300) -- ждём
		sampSendDialogResponse(sampGetCurrentDialogId(), 1, 1, nil) -- нажимаем на второй пункт диалога
		wait(300) -- ждём
		sampSendDialogResponse(sampGetCurrentDialogId(), 1, 0, nil) -- нажимаем на третий пункт диалога
		wait(300) -- ждём
		sampSendDialogResponse(sampGetCurrentDialogId(), 1, 1, nil) -- нажимаем на первый пункт диалога
		wait(300) -- ждём
		sampCloseCurrentDialogWithButton(0) -- закрываем диалог после цикла
	end)
end

function plopata2() -- вернуть лопату
	lua_thread.create(function()
	    wait(200)
		setCharCoordinates(PLAYER_PED, farms[farm].barn.x, farms[farm].barn.y, farms[farm].barn.z) -- тп к амбару
		wait(250) -- задержка после тп
        pressAlt()
		wait(300) -- ждём
		sampSendDialogResponse(sampGetCurrentDialogId(), 1, 1, nil) -- нажимаем на второй пункт диалога
		wait(300) -- ждём
		sampSendDialogResponse(sampGetCurrentDialogId(), 1, 0, nil) -- нажимаем на третий пункт диалога
		wait(300) -- ждём
		sampSendDialogResponse(sampGetCurrentDialogId(), 1, 1, nil) -- нажимаем на первый пункт диалога
		wait(300) -- ждём
		sampCloseCurrentDialogWithButton(0) -- закрываем диалог после цикла
	end)
end

function vlopata() -- взять лопату
	lua_thread.create(function()
	    wait(200)
		setCharCoordinates(PLAYER_PED, farms[farm].barn.x, farms[farm].barn.y, farms[farm].barn.z) -- тп к амбару
		wait(250) -- задержка после тп
        pressAlt()
		wait(300) -- ждём
		sampSendDialogResponse(sampGetCurrentDialogId(), 1, 1, nil) -- нажимаем на второй пункт диалога
		wait(300) -- ждём
		sampSendDialogResponse(sampGetCurrentDialogId(), 1, 0, nil) -- нажимаем на третий пункт диалога
		wait(300) -- ждём
		sampSendDialogResponse(sampGetCurrentDialogId(), 1, 0, nil) -- нажимаем на первый пункт диалога
		wait(300) -- ждём
		sampCloseCurrentDialogWithButton(0) -- закрываем диалог после цикла
	end)
end

function pgrabli() -- вернуть грабли
	lua_thread.create(function()
	    wait(200)
		setCharCoordinates(PLAYER_PED, farms[farm].barn.x, farms[farm].barn.y, farms[farm].barn.z) -- тп к амбару
		wait(300) -- задержка после тп
        pressAlt()
		wait(300) -- ждём
		sampSendDialogResponse(sampGetCurrentDialogId(), 1, 1, nil) -- нажимаем на второй пункт диалога
		wait(300) -- ждём
		sampSendDialogResponse(sampGetCurrentDialogId(), 1, 1, nil) -- нажимаем на третий пункт диалога
		wait(300) -- ждём
		sampSendDialogResponse(sampGetCurrentDialogId(), 1, 1, nil) -- нажимаем на первый пункт диалога
		wait(300) -- ждём
		sampCloseCurrentDialogWithButton(0) -- закрываем диалог после цикла
	end)
end

function vgrabli() -- взять грабли
	lua_thread.create(function()
	    wait(200)
		setCharCoordinates(PLAYER_PED, farms[farm].barn.x, farms[farm].barn.y, farms[farm].barn.z) -- тп к амбару
		wait(300) -- задержка после тп
        pressAlt()
		wait(300) -- ждём
		sampSendDialogResponse(sampGetCurrentDialogId(), 1, 1, nil) -- нажимаем на второй пункт диалога
		wait(300) -- ждём
		sampSendDialogResponse(sampGetCurrentDialogId(), 1, 1, nil) -- нажимаем на третий пункт диалога
		wait(300) -- ждём
		sampSendDialogResponse(sampGetCurrentDialogId(), 1, 0, nil) -- нажимаем на первый пункт диалога
		wait(300) -- ждём
		sampCloseCurrentDialogWithButton(0) -- закрываем диалог после цикла
	end)
end

function pvedro() -- вернуть ведро
	lua_thread.create(function()
	    wait(200)
		setCharCoordinates(PLAYER_PED, farms[farm].barn.x, farms[farm].barn.y, farms[farm].barn.z) -- тп к амбару
		wait(300) -- задержка после тп
        pressAlt()
		wait(300) -- ждём
		sampSendDialogResponse(sampGetCurrentDialogId(), 1, 1, nil) -- нажимаем на второй пункт диалога
		wait(300) -- ждём
		sampSendDialogResponse(sampGetCurrentDialogId(), 1, 2, nil) -- нажимаем на третий пункт диалога
		wait(300) -- ждём
		sampSendDialogResponse(sampGetCurrentDialogId(), 1, 1, nil) -- нажимаем на первый пункт диалога
		wait(300) -- ждём
		sampCloseCurrentDialogWithButton(0) -- закрываем диалог после цикла
	end)
end

function vvedro() -- взять ведро
	lua_thread.create(function()
	    wait(200)
		setCharCoordinates(PLAYER_PED, farms[farm].barn.x, farms[farm].barn.y, farms[farm].barn.z) -- тп к амбару
		wait(300) -- задержка после тп
        pressAlt()
		wait(300) -- ждём
		sampSendDialogResponse(sampGetCurrentDialogId(), 1, 1, nil) -- нажимаем на второй пункт диалога
		wait(300) -- ждём
		sampSendDialogResponse(sampGetCurrentDialogId(), 1, 2, nil) -- нажимаем на третий пункт диалога
		wait(300) -- ждём
		sampSendDialogResponse(sampGetCurrentDialogId(), 1, 0, nil) -- нажимаем на первый пункт диалога
        wait(300)
		sampCloseCurrentDialogWithButton(0) -- закрываем диалог после цикла
	end)
end
-- Переносные действия

--[[--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------- Хуки  --------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------]]

function sampev.onServerMessage(color, text)
	if text:find("%[Ошибка%] %{ffffff%}Сейчас нет заданий") then
		blockRabota()
	elseif text:find("%[Ошибка%] %{ffffff%}Такого саженца нет в амбаре") then
		blockRabota()
	end
end

function pressAlt()
	lua_thread.create(function()
     setVirtualKeyDown(18,true)
     wait(150)
     setVirtualKeyDown(18,false)
	 wait(150)
  end)
end

--[[--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------- Какие-то ненужные вещи. --------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------]]

function goToBoting()
	lua_thread.create(function()
		repeat wait(0) until sampIsLocalPlayerSpawned() -- скрипт не будет выполняться до спавна педа
		wait(200)
		setCharCoordinates(PLAYER_PED, farms[farm].farm.x, farms[farm].farm.y, farms[farm].farm.z)
		wait(250) -- ждём
		pressAlt()
		wait(1000)
		setCharCoordinates(PLAYER_PED, 731.26879882813, 1799.7760009766, 1602.0059814453)
		wait(200) -- ждём
		pressAlt()
		wait(200)
		sampSendDialogResponse(sampGetCurrentDialogId(), 1, 0, nil) -- нажимаем на нулевой пункт диалога
		wait(100)
		sampCloseCurrentDialogWithButton(0) -- закрываем диалог после цикла
		wait(50)
		setCharCoordinates(PLAYER_PED, 728.36968994141, 1799.6337890625, 1602.0047607422)
		wait(350) -- ждём
		pressAlt()
		active = true
	end) -- 2400
end

function goToBoting3()
	lua_thread.create(function()
		repeat wait(0) until sampIsLocalPlayerSpawned() -- скрипт не будет выполняться до спавна педа
		wait(200)
		setCharCoordinates(PLAYER_PED, farms[1].farm.x, farms[1].farm.y, farms[1].farm.z)
		wait(250) -- ждём
		pressAlt()
		wait(1000)
		setCharCoordinates(PLAYER_PED, 731.26879882813, 1799.7760009766, 1602.0059814453)
		wait(200) -- ждём
		pressAlt()
		wait(200)
		sampSendDialogResponse(sampGetCurrentDialogId(), 1, 0, nil) -- нажимаем на нулевой пункт диалога
		wait(100)
		sampCloseCurrentDialogWithButton(0) -- закрываем диалог после цикла
		wait(50)
		setCharCoordinates(PLAYER_PED, 728.36968994141, 1799.6337890625, 1602.0047607422)
		wait(350) -- ждём
		pressAlt()
		active = true
	end) -- 2400
end

function goToBoting2()
	lua_thread.create(function()
		repeat wait(0) until sampIsLocalPlayerSpawned() -- скрипт не будет выполняться до спавна педа
		wait(200)
		setCharCoordinates(PLAYER_PED, farms[2].farm.x, farms[2].farm.y, farms[2].farm.z)
		wait(250) -- ждём
		pressAlt()
		wait(1000)
		setCharCoordinates(PLAYER_PED, 731.26879882813, 1799.7760009766, 1602.0059814453)
		wait(200) -- ждём
		pressAlt()
		wait(200)
		sampSendDialogResponse(sampGetCurrentDialogId(), 1, 0, nil) -- нажимаем на нулевой пункт диалога
		wait(100)
		sampCloseCurrentDialogWithButton(0) -- закрываем диалог после цикла
		wait(50)
		setCharCoordinates(PLAYER_PED, 728.36968994141, 1799.6337890625, 1602.0047607422)
		wait(350) -- ждём
		pressAlt()
		active = true
	end) -- 2400
end

function reloadedbot() -- релоадед
	lua_thread.create(function()
	sampAddChatMessage("3", -1)
	active = false
	wait(1000)
	sampAddChatMessage("2", -1)
	active = true
	wait(1000)
	sampAddChatMessage("1", -1)
	wait(1000)
	bot()
	end)
end

function reloadedbot2() -- релоадед
	lua_thread.create(function()
	sampAddChatMessage("3", -1)
	active = false
	wait(1000)
	sampAddChatMessage("2", -1)
	active = true
	wait(1000)
	sampAddChatMessage("1", -1)
	wait(1000)
	bot1()
	end)
end
 
function test_func(arguments)
    lua_thread.create(function()
        local intArg = tonumber(arguments)
        for i = 0, intArg do
		    sampSendDialogResponse(sampGetCurrentDialogId(), 1, 10, nil)
		    --sampAddChatMessage("Blast.hk #" .. i, -1)
		    wait(100) -- ждём
    end
	end)
end 

local strings = [[{ffffff}Рожь
{ffff00}Мо{ffffff}рковь
Картофель
Лён
Хлопок
Пшеница
Огурцы
Помидоры
Белый Виноград
Чай
Пряные травы
Канабис
Кукуруза
Фиолетовый виноград
Лечебная трава
Подсолнух]]

function show_menu()
    sampShowDialog(107, "[Ferm-Poc] Change hartvel (beta)", strings, "Выбрать", "Закрыть", 2) -- создаём локальный диалог, задаём ему 1 id
end

function autoupdate(json_url, prefix, url)
  local dlstatus = require('moonloader').download_status
  local json = getWorkingDirectory() .. '\\'..thisScript().name..'-Version.json'
  if doesFileExist(json) then os.remove(json) end
  downloadUrlToFile(json_url, json,
    function(id, status, p1, p2)
      if status == dlstatus.STATUSEX_ENDDOWNLOAD then
        if doesFileExist(json) then
          local f = io.open(json, 'r')
          if f then
            local info = decodeJson(f:read('*a'))
            updatelink = info.updateurl
            updateversion = info.latest
            f:close()
            os.remove(json)
            if updateversion ~= thisScript().version then
              lua_thread.create(function(prefix)
                local dlstatus = require('moonloader').download_status
                local color = -1
                sampAddChatMessage((prefix..'Обнаружено обновление. Пытаюсь обновиться c '..thisScript().version..' на '..updateversion), color)
                wait(250)
                downloadUrlToFile(updatelink, thisScript().path,
                  function(id3, status1, p13, p23)
                    if status1 == dlstatus.STATUS_DOWNLOADINGDATA then
                      print(string.format('Загружено %d из %d.', p13, p23))
                    elseif status1 == dlstatus.STATUS_ENDDOWNLOADDATA then
                      print('Загрузка обновления завершена.')
                      sampAddChatMessage((prefix..'Обновление завершено!'), color)
                      goupdatestatus = true
                      lua_thread.create(function() wait(500) thisScript():reload() end)
                    end
                    if status1 == dlstatus.STATUSEX_ENDDOWNLOAD then
                      if goupdatestatus == nil then
                        sampAddChatMessage((prefix..'Обновление прошло неудачно. Запускаю устаревшую версию..'), color)
                        update = false
                      end
                    end
                  end
                )
                end, prefix
              )
            else
              update = false
              print('v'..thisScript().version..': Обновление не требуется.')
            end
          end
        else
          print('v'..thisScript().version..': Не могу проверить обновление. Смиритесь или проверьте самостоятельно на '..url)
          update = false
        end
      end
    end
  )
  while update ~= false do wait(100) end
end

--[[--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------- Конец --------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------]]
